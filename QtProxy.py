# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 by Matthias Höffken (author). All rights reserved.
#
# Licensed under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version..
# See LICENSE file in the project root for full license information.
# If not, see <http://www.gnu.org/licenses/>.
#
# --ANKER--


__all__ = [ \
    "QtCore", "QtGui", "Qt", "pyqtSignal", \
    "QApplication", "QMainWindow", "QGraphicsScene", "QGraphicsPixmapItem", "QMenu", \
    "QFileDialog", "QAction", "QMessageBox", "QGraphicsView", "QFrame", \
    "QImage", \
    "QImage2QPixmap_OutsideGui", "QImage2QPixmap_InsideGui" ]


USE_QT5 = False
USE_QT4 = True



if USE_QT4:
    
    try:
        
        from PyQt4 import QtCore
        from PyQt4 import QtGui
            
        QtCore.Signal = QtCore.pyqtSignal
        QtCore.Slot = QtCore.pyqtSlot
        
        
        from PyQt4.QtCore import Qt, pyqtSignal
        from PyQt4.QtGui import QApplication, QMainWindow, QGraphicsScene, QGraphicsPixmapItem, QMenu, \
                                QFileDialog, QAction, QMessageBox, QGraphicsView, QFrame
        
        from PyQt4.QtGui import QImage, QPixmap

        
        QImage2QPixmap_OutsideGui = lambda x: x
        QImage2QPixmap_InsideGui = lambda x: QPixmap.fromImage( x )

    except ImportError:
        USE_QT4 = False
        USE_QT5 = True
    except RuntimeError:
        USE_QT4 = False
        USE_QT5 = True


if USE_QT5:
    
    from PyQt5 import QtCore
    from PyQt5 import QtGui
    
    QtCore.Signal = QtCore.pyqtSignal
    QtCore.Slot = QtCore.pyqtSlot
    
    from PyQt5.QtCore import Qt, pyqtSignal
    from PyQt5.QtWidgets import QApplication, QMainWindow, QGraphicsScene, QGraphicsPixmapItem, QMenu, \
                                QFileDialog, QAction, QMessageBox, QGraphicsView, QFrame
    
    from PyQt5.QtGui import QImage, QPixmap
    
    QImage2QPixmap_OutsideGui = lambda x: QPixmap.fromImage( x )
    QImage2QPixmap_InsideGui = lambda x: x

